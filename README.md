# Ansible - Automate Nodejs application Deployment 

## Table of Contents

- [Project Description](#project-description)

- [Technologies Used](#technologies-used)

- [Steps](#steps)

- [Installation](#installation)

- [Usage](#usage)

- [How to Contribute](#how-to-contribute)

- [Questions](#questions)

- [References](#references)

- [License](#license)

## Project Description

* Used Terraform to create a cloud server(droplet) on DigitalOcean 

* Wrote Ansible Playbook that installs necessary technologies, creates Linux user for an application and deploys a NodeJS application with that user  

## Technologies Used 

* Ansible 

* Terraform 

* Node.js 

* DigitalOcean

* Linux 

## Steps

Step 1: Create Droplet(cloud server) with terraform 

[Terraform config file](/images/01_create_droplet_with_terraform_IAC.png)

[Droplet running](/images/02_droplet_running.png)

Step 2: Create hosts file for ansible and put ip address of cloud server and the path of private key and user you want access to, this will enable ansible to ssh into server

[Hosts file](/images/03_create_hosts_file_for_ansible_and_put_ip_address_of_cloud_server_and_the_path_of_private_key_and_user_you_want_access_this_is_to_enable_ansible_to_ssh_into_server.png)

Step 3: Create playbook file 

    touch deploy-node.yaml 

[Playbook file created](/images/04_create_playbook_file.png)

Step 4: You will need to create a play, to start off insert name you want to give your first play 

[Name of first play](/images/05_insert_name_you_want_to_give_the_play.png)

Step 5: insert host name or ip address you want this play to take place in

[host attribute](/images/06_add_host_name_or_ip_address_which_is_the_cloud_server.png)

Step 6: insert name of task you want to be executed on your first play 

[First play task name](/images/09_insert_name_of_task_you_want_first.png)

Step 7: call apt module with necessary parameters such as update_cache=yes, force_apt_get=yes and cache_valid_time=3000 which will delay time till cache gets updated 

[apt module update server](/images/10_call_apt_module_with_necessary_parameters_such_as_update_cache_force_apt_get_to_use_apt_get_and_lastly_cache_valid_time_which_will_delay_the_time_till_cache_gets_updated.png)

Step 8: Add new task name for new task to install nodejs and npm

[New task name install nodejs and npm](/images/11_add_new_task_name_for_new_task_to_install_nodejs_and_npm.png)

Step 9: Call apt module to install node.js and npm 

[apt module intall nodejs and npm](/images/12_call_apt_module_to_install_nodejs_and_npm.png)

Step 10: Create second play, firstly name play 

[Second play name](/images/13_create_second_play_name_new_play.png)

Step 11: Insert host name or ip address

[Second play host](/images/14_insert_the_host_name_or_address.png)

Step 12: Name new task

[Second play task name](/images/15_name_new_tasks.png)

Step 13: Use module copy which is used to copy files, insert parameters src which is the absolute path to the file on your computer and set dest parameter which is where you want the file to live on your cloud server

[Copy module](/images/16_use_module_copy_which_is_used_to_copy_files_insert_parameters_src_which_is_the_absolute_path_to_the_file_on_your_computer_and_set_dest_parameter_which_is_where_you_want_the_file_to_live_on_your_cloud_server.png)

Step 14: Nmae next step which is unpacking the file 

[Task name unpack the file](/images/17_name_next_step_which_is_unpacking_the_file.png)

Step 15: Call unarchive module and use it to unpack the file

[Unarchive module](/images/18_call_unarchive_module_and_use_it_to_unpack_the_file.png)

Step 16: Use parameter remote_src and set to yes because if you dont do this it will look for the source on your local computer instead of remote 

[Remote src](/images/19_use_parameter_remote_src_and_set_to_yes_because_if_you_dont_do_this_it_will_look_for_the_sorce_on_your_local_computer_instead_of_remote.png)

Step 17: Play changes

[1 Play Changes](/images/20_play_changes.png)

[Folder in cloud](/images/21_folder_in_cloud_server_root_user_home_directory.png)

Step 18: Clean up file, this means you can delete copy module because unarchive can copy from local computer to remote, delete parameter remote_src

[Cleaning up file deleted copy](/images/22_clean_up_file_you_can_delete_copy_module_because_unarchive_can_copy_from_local_computer_tom_remote_delete-parameter_remote-src.png)

Step 19: Create new task name for downloading dependecies 

[New task name for downloading dependencies](/images/23_create_new_task_name_for_downloading_dependecies.png)

Step 20: Call npm module 

[NPM module](/images/24_call_npm_module.png)

Step 21: Use path parameter and put the path of application direcotry on remote server

[Path Parameter](/images/25_use_path_parameter_and_put_the_path_of_application_directory_on_remote_server.png)

Step 22: Create a new task to start application and in this task use the command module to use node bcause they are no modules for node

[Command module](/images/26_create_a_new_task_to_start_application_use_the_command_module_to_use_node_because_they_are_no_modules_for-nodes.png)

Step 23: Use attributes async and poll to run command in detached mode so it doesnt block the terminal (running in detached mode) 

[async and poll](/images/27_use_attributes_async_and_poll_to_run_command_in_detached_mode_so_it_doesnt_block_the_terminal.png)

Step 24: Test the playbook 

[Successful playbook test](/images/28_tasks_were_successful.png)

[Process running on remote](/images/29_process_running_on_remote_server.png)

Step 25: Use module shell to check if the process was successful then use register to assign the output to a variable. After doing that use debug to display the output and stdout_lines for the specific output of the variable 

[Module shell](/images/30_use_module_shell_to_check_if_the_process_was_successful_then_use_register_to_assign_the_output_to_a_variable_lastly_use_debug_to_display_the_output_and_stdout_lines_for_the_specific_output_of_the_variable.png)

Step 26: Test play 

[Successful play with shell](/images/31_successful_output.png)

Step 27: For security best practices create a new play that cretaes linux user, so the next step is to name the play

[New play Linux user](/images/32_for_security_best_practice_create_a_new_play_that_create_linux_user___name_lay.png)

Step 28: set host

[Host linux user](/images/33_insert_host.png)

Step 30: Create a task with appropriate parameter to create user like name, group you want user to belong in and comment

[User parameters](/images/34_create_task_that_with_appropriate_parameters_to_create_user_like_name_of_user_group_you_want_user_to_belong_to_and_comment.png)

Step 31: Modify playbook by changing root to user that you created 

[Change root to created user](/images/35_modify_application_by_changing_root_to_user_that_you_created.png)

Step 32: Before executing task insert become_user attribute and set to user you want to become after that set become to true because it is set to false by default 

[Become user](/images/36_before_executing_task_insert_become_user_attribute_and_set_to_user_you_want_to_become_after_that_set_become_to_true_because_it_is_set_to_false_by_default.png)

Step 33: Execute playbook

[Last playbook test](/images/37_execute_playbook.png)

## Installation

    brew install ansible 

## Usage 

    ansible-playbook -i host deploy-node.yaml 

## How to Contribute

1. Clone the repo using $ git clone git@gitlab.com:omacodes98/ansible-automate-node.js-application-deployment.git

2. Create a new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review



## Questions

Feel free to contact me for further questions via: 

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com

## References

https://gitlab.com/omacodes98/ansible-automate-node.js-application-deployment

## License

The MIT License 

For more informaation you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji.