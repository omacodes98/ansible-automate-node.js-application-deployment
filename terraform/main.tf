provider "digitalocean" {
  token = var.do_token
}

variable region {}
variable do_token {}
variable image_id {}
variable size_id {}
variable ssh_keys {}

resource "digitalocean_droplet" "nodejs-app-server" {
    image = var.image_id
    name = "nodejs-app-server"
    region = var.region
    size = var.size_id
    ssh_keys = var.ssh_keys
}

output "droplet_public_ip" {
    value = digitalocean_droplet.nodejs-app-server.ipv4_address
}